

* overview


| prepare medium      |                                 |
|                     |                                 |
| before login        |                                 |
|                     | build xz                        |
|                     | mount, extract xz, chroot       |
|                     | fstab                           |
|                     | grub, passwd, timezone, adduser |
|                     |                                 |
| after login         |                                 |
|                     | home                            |
|                     | xinitrc                         |
|                     | spectrwm.conf                   |
| scrot               |                                 |
| ssh                 |                                 |
| docker              |                                 |
| local.org           |                                 |
| emacs customization |                                 |
|                     |                                 |


* prepare medium

** 0

|    |           |   |
| 10 | parted    |   |
|    |           |   |
| 20 | mkfs.ext4 |   |
|    |           |   |


** 10


parted -a optimal /dev/sdc


mkpart primary 0 1 


** 20


mkfs.ext4 /dev/sdc2


mkfs.ext4 /dev/sdc3


mkfs.ext4 /dev/sdc4


* before login

** 0

|    |                                 |
| 10 | build xz                        |
|    |                                 |
| 20 | mount, extract xz, chroot       |
|    |                                 |
| 30 | fstab                           |
|    |                                 |
| 40 | grub, passwd, timezone, adduser |



** 10


#    g=cmch/gf104:docker


#    g=cmch/gf104:base


docker run -it --rm \
    -v /tmp:/tmp \
    $g



cat <<EOF >  /tmp/stage4.excl
.bash_history
/mnt/*
/tmp/*
/proc/*
/sys/*
/dev/*
EOF


#  cat /tmp/stage4.excl


time nice -10 \
tar -X /tmp/stage4.excl -c / | xz -1vT0  > /tmp/mydebian.xz


[2020-02-04 Tue 17:24]


real    0m42.303s
user    1m27.950s
sys     0m4.788s



** 20


#   umount /tmp/installusb/mnt/local


#   umount /tmp/installusb/boot


#   umount /tmp/installusb


#   e=/dev/sda


#   e=/dev/sdb


#   e=/dev/sdc


#   e=/dev/sdd



d=/tmp/installusb


mkdir $d


#    mount ${e}3 $d


#    mount ${e}4 $d


#    mount ${e}5 $d



#    mkdir $d/boot


#    echo ${e}2



mount ${e}2 $d/boot


#  mkdir -p $d/mnt/local 


#  mount ${e}3 $d/mnt/local


#  mount ${e}4 $d/mnt/local


mount ${e}5 $d/mnt/local


#  mount ${e}6 $d/mnt/local



lsblk



ls /tmp/my*  -l


f=/tmp/mydebian.xz


ls -lh $f


time nice -10 \
tar xvpf $f \
-C $d --xattrs


[2020-02-04 Tue 17:27]


real    0m17.037s
user    0m15.691s
sys     0m4.294s





mount -t proc none $d/proc
mount -o bind /sys $d/sys
mount -o bind /dev $d/dev
mount --bind /dev/pts $d/dev/pts
chroot $d /bin/sh


exit
umount $d/dev -l
umount $d/sys -l
umount $d/proc
umount $d/dev/pts



** 30


#    d=sda

#    d=sdb

#    d=sdc

#    d=sdd


blkid | awk -v var=$d '$0 ~ var {print $1, $2}'


b=$(  blkid | awk -v v=${d}2 -F\" '$1 ~ v {print $2}'  ); \
echo; echo -n "boot device UUID" ${d}2 $b ; echo; echo


# 3 root
r=$(  blkid | awk -v v=${d}3 -F\" '$1 ~ v {print $2}'  ); \
echo; echo -n "root device UUID" ${d}3 $r; echo; echo


# 4 root
r=$(  blkid | awk -v v=${d}4 -F\" '$1 ~ v {print $2}'  ); \
echo; echo -n "root device UUID" ${d}4 $r; echo; echo


# 5
r=$(  blkid | awk -v v=${d}5 -F\" '$1 ~ v {print $2}'  ); \
echo; echo -n "root device UUID" ${d}5 $r; echo; echo


# 3 usb local
u=$(  blkid | awk -v v=${d}3 -F\" '$1 ~ v {print $2}'  ); \
echo; echo -n "usb device UUID" ${d}3 $u; echo; echo


# 4 usb local
u=$(  blkid | awk -v v=${d}4 -F\" '$1 ~ v {print $2}'  ); \
echo; echo -n "usb device UUID" ${d}4 $u; echo; echo


# 6 usb local
u=$(  blkid | awk -v v=${d}6 -F\" '$1 ~ v {print $2}'  ); \
echo; echo -n "usb device UUID" ${d}6 $u; echo; echo


# 5 usb local
u=$(  blkid | awk -v v=${d}5 -F\" '$1 ~ v {print $2}'  ); \
echo; echo -n "usb device UUID" ${d}5 $u; echo; echo


cat  <<EOF > /etc/fstab 
UUID=$r    /            ext4    noatime              0 1
UUID=$b   /boot        ext4    defaults,noatime     0 2

UUID=$u   /mnt/local        ext4    defaults,noatime     0 2
tmpfs /tmp         tmpfs rw,nosuid,noatime,nodev,mode=1777 0 0
EOF

cat /etc/fstab  




** 40


| grub-install            |
|                         |
| update-grub             |
|                         |
| dpkg-reconfigure tzdata |
|                         |
| passwd                  |
|                         |
| adduser                 |
|                         |


#   grub-install /dev/sda


#   grub-install /dev/sdb


#   grub-install /dev/sdc


#   grub-install /dev/sdd


update-grub



#  errors

grub-install: warning: Attempting to install GRUB to a disk with multiple partition labe
grub-install: warning: Embedding is not possible.  GRUB can only be installed in this se
grub-install: error: will not proceed with blocklists.
 

parted -a optimal /dev/sda


mkpart primary 0 1 


quit


lsblk


dd if=/dev/zero of=/dev/sda6


parted -a optimal /dev/sda


rm 6


quit


lablk


dpkg-reconfigure tzdata


13. Etc


28. GMT-8


# see systemd-networkd


 systemctl enable systemd-networkd



f=/etc/systemd/network/20-wired.network


cat <<EOF > $f

[Match]
Name=enp*

[Network]
DHCP=ipv4

EOF

cat $f



#   echo yusb-202002 > /etc/hostname


#   echo yusb-201904 > /etc/hostname


#   echo rusb-201907 > /etc/hostname


#   echo wusb-201909 > /etc/hostname


#   echo ssd-201909 > /etc/hostname


#   echo 280g-201909 > /etc/hostname


passwd


adduser user2



usermod -aG docker user2


* flow

** 0

| v | 10 | build xz                        |
|   |    |                                 |
| v | 20 | mount, extract xz, chroot       |
|   |    |                                 |
| v | 30 | fstab                           |
|   |    |                                 |
| v | 40 | grub, passwd, timezone, adduser |
|   |    |                                 |
| v | 50 | xinitrc                         |
|   |    |                                 |
| v | 52 | .spectrwm.conf                  |
|   |    |                                 |
|   | 54 | baraction.sh                    |
|   |    |                                 |
|   | 56 | scrot                           |
|   |    |                                 |
|   | 60 | docker environment              |
|   |    |                                 |
|   | 62 | prepare docker images           |
|   |    |                                 |
|   | 70 | prepare docker client home      |
|   |    |                                 |
|   | 72 | local.org                       |
|   |    |                                 |
|   | 74 | .ssh                            |
|   |    |                                 |
|   | 80 | emacs customization             |
|   |    |                                 |


** 10


f=cmch/gf104:nvidia


docker run -it --rm \
    -v /tmp:/tmp \
    $f


cat <<EOF >  /tmp/stage4.excl
.bash_history
/mnt/*
/tmp/*
/proc/*
/sys/*
/dev/*
EOF


#  cat /tmp/stage4.excl


time nice -10 \
tar -X /tmp/stage4.excl -c / | xz -1vT0  > /tmp/mydebian.xz


[2020-01-22 Wed 16:12]


  100 %      348.8 MiB / 1205.9 MiB = 0.289    29 MiB/s       0:42             

real    0m42.296s
user    2m27.782s
sys     0m3.095s



[2019-09-15 Sun 23:36]


 100 %      363.0 MiB / 1332.1 MiB = 0.272    11 MiB/s       1:59             

real    1m59.432s
user    3m26.148s
sys     0m7.400s


** 20


#   umount /tmp/installusb/mnt/local


#   umount /tmp/installusb/boot


#   umount /tmp/installusb


#   e=/dev/sda


#   e=/dev/sdb


#   e=/dev/sdc


#   e=/dev/sdd



d=/tmp/installusb


mkdir $d


#    mount ${e}3 $d


#    mount ${e}4 $d


#    mount ${e}5 $d



#    mkdir $d/boot


#    echo ${e}2



mount ${e}2 $d/boot


#  mkdir -p $d/mnt/local 


#  mount ${e}4 $d/mnt/local


mount ${e}5 $d/mnt/local


#  mount ${e}6 $d/mnt/local



lsblk



ls /tmp/my*  -l


f=/tmp/mydebian.xz


ls -lh $f


time nice -10 \
tar xvpf $f \
-C $d --xattrs


[2020-01-22 Wed 16:15]

real    0m45.155s
user    0m26.684s
sys     0m7.938s



[2019-09-16 Mon 10:20]

real    0m36.651s
user    0m34.464s
sys     0m9.844s


/dev/sda5       9.4G  1.5G  7.5G  17% /tmp/installusb



[2019-09-15 Sun 23:37]


real    0m51.155s
user    0m39.236s
sys     0m8.120s


[2019-09-09 Mon 15:55]


real    0m56.913s
user    0m33.420s
sys     0m10.188s


df -h

Filesystem      Size  Used Avail Use% Mounted on

/dev/sda3       2.7G  1.3G  1.3G  50% /tmp/installusb



mount -t proc none $d/proc
mount -o bind /sys $d/sys
mount -o bind /dev $d/dev
mount --bind /dev/pts $d/dev/pts
chroot $d /bin/sh


exit
umount $d/dev -l
umount $d/sys -l
umount $d/proc
umount $d/dev/pts



** 30


#    d=sda

#    d=sdb

#    d=sdc

#    d=sdd


blkid | awk -v var=$d '$0 ~ var {print $1, $2}'


b=$(  blkid | awk -v v=${d}2 -F\" '$1 ~ v {print $2}'  ); \
echo; echo -n "boot device UUID" ${d}2 $b ; echo; echo


# 3 root
r=$(  blkid | awk -v v=${d}3 -F\" '$1 ~ v {print $2}'  ); \
echo; echo -n "root device UUID" ${d}3 $r; echo; echo


# 4 root
r=$(  blkid | awk -v v=${d}4 -F\" '$1 ~ v {print $2}'  ); \
echo; echo -n "root device UUID" ${d}4 $r; echo; echo


# 5
r=$(  blkid | awk -v v=${d}5 -F\" '$1 ~ v {print $2}'  ); \
echo; echo -n "root device UUID" ${d}5 $r; echo; echo


# 4 usb local
u=$(  blkid | awk -v v=${d}4 -F\" '$1 ~ v {print $2}'  ); \
echo; echo -n "usb device UUID" ${d}4 $u; echo; echo


# 6 usb local
u=$(  blkid | awk -v v=${d}6 -F\" '$1 ~ v {print $2}'  ); \
echo; echo -n "usb device UUID" ${d}6 $u; echo; echo


# 5 usb local
u=$(  blkid | awk -v v=${d}5 -F\" '$1 ~ v {print $2}'  ); \
echo; echo -n "usb device UUID" ${d}5 $u; echo; echo


cat  <<EOF > /etc/fstab 
UUID=$r    /            ext4    noatime              0 1
UUID=$b   /boot        ext4    defaults,noatime     0 2

UUID=$u   /mnt/local        ext4    defaults,noatime     0 2
tmpfs /tmp         tmpfs rw,nosuid,noatime,nodev,mode=1777 0 0
EOF

cat /etc/fstab  




** 40


| grub-install            |
|                         |
| update-grub             |
|                         |
| dpkg-reconfigure tzdata |
|                         |
| passwd                  |
|                         |
| adduser                 |
|                         |


#   grub-install /dev/sda


#   grub-install /dev/sdb


#   grub-install /dev/sdc


#   grub-install /dev/sdd


update-grub



#  errors

grub-install: warning: Attempting to install GRUB to a disk with multiple partition labe
grub-install: warning: Embedding is not possible.  GRUB can only be installed in this se
grub-install: error: will not proceed with blocklists.
 

parted -a optimal /dev/sda


mkpart primary 0 1 


quit


lsblk


dd if=/dev/zero of=/dev/sda6


parted -a optimal /dev/sda


rm 6


quit


lablk


dpkg-reconfigure tzdata


13. Etc


28. GMT-8


# see systemd-networkd


 systemctl enable systemd-networkd



f=/etc/systemd/network/20-wired.network


cat <<EOF > $f

[Match]
Name=enp*

[Network]
DHCP=ipv4

EOF

cat $f



#   echo yusb-202002 > /etc/hostname


#   echo rusb-201907 > /etc/hostname


#   echo wusb-201909 > /etc/hostname


#   echo ssd-201909 > /etc/hostname


#   echo 280g-201909 > /etc/hostname


passwd


adduser user


usermod -aG docker user



** 50


f=/home/user/.xinitrc

cat <<EOF > $f

# b1 nvidia
xrandr --output DVI-I-3 --left-of DVI-I-2

# 14 intel
xrandr --output HDMI-1 --left-of VGA-1

exec spectrwm

EOF


# cat $f





** 52


# cp /etc/spectrwm.conf  /home/user/.spectrwm.conf

f=/home/user/.spectrwm.conf

cat <<EOF  > $f
modkey = Mod4
bar_at_bottom         = 1
program[term]= xterm -fg white -bg black
# workspace_limit= 6
bar_format		= +N:+I +S +2<%d %T +2<+A+4<+V
bar_action = baraction.sh
EOF

#  cat $f






** 54


f=/home/user/baraction.sh 

cat <<EOF  > $f
#!/bin/bash
# baraction.sh script for spectrwm status bar

SLEEP_SEC=5  # set bar_delay = 5 in /etc/spectrwm.conf
COUNT=0
#loops forever outputting a line every SLEEP_SEC secs
while :; do
    LOAD=\$(uptime | sed 's/.*://; s/,//g')
    echo -n \$LOAD ""

    m=\$(  free -h | awk '\$0 ~ /Mem/ {print \$NF}'  ); echo -n "ava" \$m ""
        
    s=\$(ps -e | grep -w 'ssh\$' | awk '{print \$1}'); echo -n "ssh" \$s ""
    
    a=\$(ip a | awk '\$0 ~ /global/ {print \$NF, \$2}' | sed 's/\/..//')
    echo -n \$a ""

    t=\$( find /sys/devices -name *temp*input* -perm 444 | xargs cat )
    for i in \$t; do
        echo -n \$( expr \$i / 1000 ) ""
    done
    echo -n "°C" ""
    
    echo
    sleep \$SLEEP_SEC
done
EOF

# cat $f


ls -lha $f


chown user:user $f


chmod 755 $f


ls -lha $f


#  ln -s {source-filename} {symbolic-filename}


ln -s $f /usr/bin/baraction.sh 


ls -l $f /usr/bin/baraction.sh 


** 56
    
*** 0

| 2 | find the name of docker client chromium |
|   |                                         |
| 4 | script test                             |
|   |                                         |
| 6 | /usr/bin/screenshot.sh                  |
|   |                                         |
| 8 | spectrwm.conf                           |
|   |                                         |
| 9 | reset spectrwm and screenshot           |


*** 2


docker ps --format "table {{.Names}}\t{{.Image}}"


# ###

docker ps 

docker ps --format "table {{.ID}}\t{{.Labels}}"

docker ps --format "{{.Names}}"


docker ps --format "{{.Names}}{{.Image}}"

docker ps --format "table {{.Names}}\t{{.Image}}"


https://docs.docker.com/engine/reference/commandline/ps/#formatting


*** 4


| test scrot command          |
|                             |
| test open image in chromium |


# capture a shot, go inside browser and open it in /tmp
scrot -s '/tmp/scrot-%Y-%m-%d_$wx$h.png' 


# capture and open in docker client chromium
scrot -s '/tmp/scrot-%Y-%m-%d_$wx$h.png'  -e ' docker exec chromium chromium-browser --disable-gpu --user-data-dir=/tmp/data --no-sandbox  $f'


*** 6


# as root, at host


f=/usr/bin/screenshot.sh


nano $f

#  #####


screenshot() {
	case $1 in
	full)
		scrot -m
		;;
	window)
		sleep 1
		#  scrot -s
		scrot -s '/tmp/scrot-%Y-%m-%d_$wx$h.png'  -e ' docker exec chromium chromium-browser --disable-gpu --user-data-dir=/tmp/data --no-sandbox  $f'
		;;
	*)
		;;
	esac;
}

screenshot $1


#  ####


cat $f


ls -lha $f


chmod +x $f


ls -lha $f


*** 8


f=/home/user/.spectrwm.conf


ls -lha $f


#   cat $f


cat $f | grep screenshot


# test ln to home2018
cat <<EOF >> $f

program[screenshot_wind]	= screenshot.sh window

EOF


*** 9


| reset spectrwm online | modkey q       |
|                       |                |
| screenshot            | modkey shift s |
|                       |                |



** 60

| flow.org |
| docker   |



** 62

| ssh      | cmch/ssh              |
|          |                       |
| proxy    | cmch/privoxy          |
|          |                       |
| emacs    | cmch/emacs:10-slim    |
|          |                       |
| chromium | cmch/chromium:10-slim |
|          |                       |


# at host as root


#    d=/mnt/local/docker-images-201909


mkdir $d


chown 166536:166536 $d -R


# autossh


#    i=cmch/ssh


#    i=cmch/privoxy


#    i=cmch/emacs:10-slim


#    i=cmch/chromium:10-slim


docker pull $i


#    docker save --output ${d}/ssh  $i


#    docker save --output ${d}/privoxy  $i


#    docker save --output ${d}/emacs  $i


#    docker save --output ${d}/chromium  $i


chown 166536:166536 $d -R


ls -lha $d




#  cp -rp /home/my_home /media/backup/my_home


s=/mnt/local/home201808/.ssh


d=/tmp/installusb/mnt/local/.ssh


cp -rp $s $d



# privoxy



docker pull c5766/privoxy


d=/tmp/installusb/mnt/local/dockerimages


docker save --output ${d}/privoxy  c5766/privoxy



ls -lha /tmp/installusb/mnt/local/dockerimages


# emacs



docker pull c5766/texlive


d=/tmp/installusb/mnt/local/dockerimages


docker save --output ${d}/texlive  c5766/texlive



ls -lha /tmp/installusb/mnt/local/dockerimages



# chromium 76



docker pull c5766/chromium


d=/tmp/installusb/mnt/local/dockerimages


docker save --output ${d}/chromium  c5766/chromium



ls -lha /tmp/installusb/mnt/local/dockerimages



** 70



d=/mnt/local/home201909


mkdir $d


chown 166536:166536 $d 


** 72


#    docker load -i [tar file]


docker network create my-network


#    ls -l /mnt/local/docker-images-201909


#    f=/mnt/local/docker-images-201909/ssh


#    f=/mnt/local/docker-images-201909/privoxy


#    f=/mnt/local/docker-images-201909/emacs


#    f=/mnt/local/docker-images-201909/chromium


docker load -i $f



** 74


#  cp -rp /home/my_home /media/backup/my_home


#  ls -l /mnt/local


s=/mnt/local/home201808/.ssh


d=/mnt/local/home201909/.ssh


cp -rp $s $d


** 80


#  No org-babel-execute function for sh!


org-babel-load-languages


#  this sh code block on your system? (yes or no) 


org-confirm-babel-evaluate


#  reverse-video


emacs --reverse-video local.org


#  menu tool bar


menu-bar-mode


tool-bar-mode


#  chinese font


(set-fontset-font (frame-parameter nil 'font)
      'han '("Noto Sans TC"))

(setq face-font-rescale-alist '(("Noto Sans TC" . 1.2)))


#  magit

(require 'package)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)


https://magit.vc/manual/magit/Installing-from-Melpa.html#Installing-from-Melpa




* after login

** 0

|    |    |                            |
| 50 | 10 | xinitrc                    |
|    |    |                            |
| 52 | 20 | .spectrwm.conf             |
|    |    |                            |
| 54 | 30 | baraction.sh               |
|    |    |                            |
| 56 | 40 | scrot                      |
|    |    |                            |
| 60 |    | docker environment         |
|    |    |                            |
| 62 |    | prepare docker images      |
|    |    |                            |
| 70 |    | prepare docker client home |
|    |    |                            |
| 72 |    | local.org                  |
|    |    |                            |
| 74 |    | .ssh                       |
|    |    |                            |
| 80 |    | emacs customization        |
|    |    |                            |


** 10


f=/home/user2/.xinitrc

cat <<EOF > $f

# b1 nvidia
xrandr --output DVI-I-3 --left-of DVI-I-2

# 14 intel
xrandr --output HDMI-1 --left-of VGA-1

exec spectrwm

EOF


# cat $f





** 20


# cp /etc/spectrwm.conf  /home/user/.spectrwm.conf

f=/home/user2/.spectrwm.conf

cat <<EOF  > $f
modkey = Mod4
bar_at_bottom         = 1
program[term]= xterm -fg white -bg black
# workspace_limit= 6
bar_format		= +N:+I +S +2<%d %T +2<+A+4<+V
bar_action = baraction.sh
EOF

#  cat $f






** 30


f=/home/user2/baraction.sh 

cat <<EOF  > $f
#!/bin/bash
# baraction.sh script for spectrwm status bar

SLEEP_SEC=5  # set bar_delay = 5 in /etc/spectrwm.conf
COUNT=0
#loops forever outputting a line every SLEEP_SEC secs
while :; do
    LOAD=\$(uptime | sed 's/.*://; s/,//g')
    echo -n \$LOAD ""

    m=\$(  free -h | awk '\$0 ~ /Mem/ {print \$NF}'  ); echo -n "ava" \$m ""
        
    s=\$(ps -e | grep -w 'ssh\$' | awk '{print \$1}'); echo -n "ssh" \$s ""
    
    a=\$(ip a | awk '\$0 ~ /global/ {print \$NF, \$2}' | sed 's/\/..//')
    echo -n \$a ""

    t=\$( find /sys/devices -name *temp*input* -perm 444 | xargs cat )
    for i in \$t; do
        echo -n \$( expr \$i / 1000 ) ""
    done
    echo -n "°C" ""
    
    echo
    sleep \$SLEEP_SEC
done
EOF

# cat $f


ls -lha $f


chown user2:user2 $f


chmod 755 $f


ls -lha $f


#  ln -s {source-filename} {symbolic-filename}


ln -s $f /usr/bin/baraction.sh 


ls -l $f /usr/bin/baraction.sh 




* scrot
   
** 0

| 20 | find the name of docker client chromium |
|    |                                         |
| 40 | script test                             |
|    |                                         |
| 60 | /usr/bin/screenshot.sh                  |
|    |                                         |
| 80 | spectrwm.conf                           |
|    |                                         |
| 90 | reset spectrwm and screenshot           |
|    |                                         |


** 20


docker ps --format "table {{.Names}}\t{{.Image}}"


# ###

docker ps 

docker ps --format "table {{.ID}}\t{{.Labels}}"

docker ps --format "{{.Names}}"


docker ps --format "{{.Names}}{{.Image}}"

docker ps --format "table {{.Names}}\t{{.Image}}"


https://docs.docker.com/engine/reference/commandline/ps/#formatting


** 40


| test scrot command          |
|                             |
| test open image in chromium |


# capture a shot, go inside browser and open it in /tmp
scrot -s '/tmp/scrot-%Y-%m-%d_$wx$h.png' 


# capture and open in docker client chromium
scrot -s '/tmp/scrot-%Y-%m-%d_$wx$h.png'  -e ' docker exec chromium chromium-browser --disable-gpu --user-data-dir=/tmp/data --no-sandbox  $f'


** 60


# as root, at host


f=/usr/bin/screenshot.sh


nano $f

#  #####


screenshot() {
	case $1 in
	full)
		scrot -m
		;;
	window)
		sleep 1
		#  scrot -s
		scrot -s '/tmp/scrot-%Y-%m-%d_$wx$h.png'  -e ' docker exec chromium chromium-browser --disable-gpu --user-data-dir=/tmp/data --no-sandbox  $f'
		;;
	*)
		;;
	esac;
}

screenshot $1


#  ####


cat $f


ls -lha $f


chmod +x $f


ls -lha $f


** 80


f=/home/user/.spectrwm.conf


ls -lha $f


#   cat $f


cat $f | grep screenshot


# test ln to home2018
cat <<EOF >> $f

program[screenshot_wind]	= screenshot.sh window

EOF


** 90


| reset spectrwm online | modkey q       |
|                       |                |
| screenshot            | modkey shift s |
|                       |                |



* docker


** 0

 
| 10 | data-root                        |
|    |                                  |
|    | userns-remap                     |
|    |                                  |
| 30 | HTTP_PROXY                       |
|    |                                  |
| 40 | userns                           |
|    |                                  |
| 50 | chrome.json                      |
|    |                                  |
| 60 | Use user-defined bridge networks |
|    |                                  |
| 70 |                                  |



** 10 


#  mkdir /etc/docker


f=/etc/docker/daemon.json


# 14, b1
cat << EOF > $f
{
    "data-root": "/mnt/local/docker-data-201909",
    "userns-remap": "default"
}
EOF


# 14, b1
cat << EOF > $f
{
    "data-root": "/mnt/local/docker-data-202002",
    "userns-remap": "default"
}
EOF


** 30



# http proxy

mkdir -p /etc/systemd/system/docker.service.d


#    ls -lha  /etc/systemd/system/docker.service.d


f=/etc/systemd/system/docker.service.d/http-proxy.conf


#  [2018-08-28 Tue 15:00] b1
cat << EOF > $f
[Service]
Environment="HTTP_PROXY=http://172.18.0.20:8118/"
EOF
#  cat $f


** 40



echo 'kernel.unprivileged_userns_clone=1' > /etc/sysctl.d/00-local-userns.conf


** 50


# at host terminal as root, cp chrome.json 


ls /mnt/local -l


cp /mnt/local/chrome.json /tmp/installusb/mnt/local/



Control Docker with systemd


https://docs.docker.com/config/daemon/systemd/







** 60


$


docker network create my-network


docker network inspect my-network


[
    {
        "Name": "my-network",
        "Id": "fd5d952574aeecf0271851ed212b5718e51e1b71d1eb53a213ccc1bb215ab1eb",
        "Created": "2019-10-29T09:13:53.546923684Z",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "172.18.0.0/16",
                    "Gateway": "172.18.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {},
        "Options": {},
        "Labels": {}
    }
]


Use user-defined bridge networks


https://docs.docker.com/network/network-tutorial-standalone/


$ docker network create --driver bridge alpine-net



